# More Lights for Foundry VTT

Currently just adds an alternative torch light animation.

Might be extended in the future with more light types.


## Installation

Copy this link and use it in Foundrys module manager to install the module
    > https://gitlab.com/henry4k/foundryvtt-more-lights/-/raw/main/module.json
