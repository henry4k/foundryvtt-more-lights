const { src, dest, watch, parallel, series } = require('gulp')
const ts  = require('gulp-typescript')
const zip = require('gulp-zip')

//let generateSourceMaps = true
//let compress = false
const project = ts.createProject('tsconfig.json')

const script = () =>
    src('src/**/*.ts')
    .pipe(project())
    .pipe(dest('scripts/'))

const pack = () =>
    src(['module.json',
         'scripts/*.js',
         'lang/*.json'], {base: './'})
    .pipe(zip('more-lights.zip'))
    .pipe(dest('./'))

const defaultTask = parallel(script)

exports.default = defaultTask

exports.watch = function(cb) {
    defaultTask(cb)
    watch('src/**/*.ts', script)
}

exports.release = function(cb) {
    //generateSourceMaps = false
    //compress = true
    defaultTask(cb)
}

exports['package-release'] = function(cb) {
    //generateSourceMaps = false
    //compress = true
    series(
        defaultTask,
        pack)(cb)
}
