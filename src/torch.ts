import { LightAnimation } from './utils.js'
import { fbm, fbmZero, fbmLimit, mix, clamp } from './math.js'

type AdaptiveIlluminationOrColorationShader =
    ConstructorOf<AdaptiveLightingShader> & {
        defaultUniforms: object
        SHADER_HEADER: string
        DISTANCE: string
    }

const TorchShaderSharedMixin = <TBase extends AdaptiveIlluminationOrColorationShader>(Base: TBase) => {
    return class Mixed extends Base {
        static override defaultUniforms = Object.assign({}, super.defaultUniforms, {
            torchOrigin: [0.5, 0.5],
            torchDistanceOffset: 0.0,
            torchBrightness: 0.0
        })

        static override SHADER_HEADER = `
        ${super.SHADER_HEADER}
        uniform vec2 torchOrigin;
        uniform float torchDistanceOffset;
        uniform float torchBrightness;
        `

        static override DISTANCE = `
        dist = clamp(distance(vUvs, torchOrigin) * 2.0 + torchDistanceOffset, 0.0, 1.0);
        `
    }
}

class TorchIlluminationShader extends TorchShaderSharedMixin(AdaptiveIlluminationShader) {
    static override fragmentShader = `
    ${this.SHADER_HEADER}
    ${this.PERCEIVED_BRIGHTNESS}

    void main() {
      ${(this as any).FRAGMENT_BEGIN}
      ${this.DISTANCE}
      ${this.TRANSITION}
      finalColor *= torchBrightness;
      ${this.ADJUSTMENTS}
      ${this.FALLOFF}
      ${(this as any).FRAGMENT_END}
    }`
}

class TorchColorationShader extends TorchShaderSharedMixin(AdaptiveColorationShader) {
    static override fragmentShader = `
    ${this.SHADER_HEADER}
    ${this.PERCEIVED_BRIGHTNESS}
    float torchfade(in float dist, in float fadepower) {
        return pow(1.0 - dist, fadepower);
    }

    void main() {
      ${(this as any).FRAGMENT_BEGIN}
      ${this.DISTANCE}
      finalColor = color * torchfade(dist , 0.9) * 0.75 * torchBrightness;
      ${this.COLORATION_TECHNIQUES}
      ${this.ADJUSTMENTS}
      ${this.FALLOFF}
      ${(this as any).FRAGMENT_END}
    }
    `
}

type PixiMesh = PIXI.Mesh & PIXI.UniformGroup

const AVERAGE_FLAME_INTENSITY = fbmLimit(4, 0.5) / 2
const MAX_ORIGIN_OFFSET_FACTOR = fbmLimit(4, 0.5)

//const MAX_INTENSITY_MAX_ORIGIN_OFFSET = 0.1
const MAX_INTENSITY_MAX_ORIGIN_OFFSET = 0.06

/**
 * @param intensity [1,10]
 *
 * @returns Theoretically [0,.5]
 */
const getMaxOriginOffset = (intensity: number) => {
    const i = intensity / 10 // [.1, 1]
    return MAX_INTENSITY_MAX_ORIGIN_OFFSET * i * i
}

export const torchLightAnimation: LightAnimation = {
    label: 'MORE_LIGHTS.Torch',
    illuminationShader: TorchIlluminationShader,
    colorationShader: TorchColorationShader,
    animation: function(dt, {speed=5, intensity=5, reverse=false}={}) {
        const lightSource = this as LightSource

        // Determine the animation timing
        let t = canvas!.app!.ticker.lastTime
        if(reverse)
            t *= -1
        const time = ((speed * t)/3000) + lightSource.animation.seed!

        lightSource.animation.time = time

        // Define parameters
        const flameIntensity = mix(AVERAGE_FLAME_INTENSITY,
                                   fbm(time + 12.34,
                                       time + 43.21,
                                       4, 0.5),
                                   (intensity+1) / 11) // t: [.18,1]; flameIntensity: [0,~1]

        const maxOriginOffset = getMaxOriginOffset(intensity)
        //const fbm2d = [fbmZero(time, 0.0, 2, 0.5) / MAX_ORIGIN_OFFSET_FACTOR,
        //               fbmZero(0.0, time, 2, 0.5) / MAX_ORIGIN_OFFSET_FACTOR] // [-1,+1]
        //const origin = [0.5 + fbm2d[0]! * maxOriginOffset,
        //                0.5 + fbm2d[1]! * maxOriginOffset]

        const fbm2d = [fbmZero(time, 0.0, 4, 0.5) / MAX_ORIGIN_OFFSET_FACTOR,
                       fbmZero(0.0, time, 4, 0.5) / MAX_ORIGIN_OFFSET_FACTOR] // [-1,+1]
        const fbm2dRescaled = [clamp(fbm2d[0]! / 0.65, -1, +1),
                               clamp(fbm2d[1]! / 0.65, -1, +1)] // [-1,+1] (but 0.65 was mapped to 1.0)
        const origin = [0.5 + fbm2dRescaled[0]! * maxOriginOffset,
                        0.5 + fbm2dRescaled[1]! * maxOriginOffset]

        const brightness = 1.0 - flameIntensity * 0.2
        const scaleOffset = flameIntensity * 0.1
        const constDistanceOffset = maxOriginOffset * Math.SQRT2 * 2
        const distanceOffset = constDistanceOffset //+ scaleOffset
        // maxOriginOffset [0,0.5]
        // ratio [0,1]
        // map [0,0.5] -> [1,2]
        // ratio *= [1,2]
        const originOffsetInv = (0.5 - constDistanceOffset) * 2 // [0,1]
        const ratio = Math.pow(lightSource.ratio!, Math.pow(originOffsetInv, 0.5))

        // Update uniforms
        const co = (lightSource.coloration   as PixiMesh).uniforms
        const il = (lightSource.illumination as PixiMesh).uniforms
        for(const uniforms of [co, il]) {
            uniforms.intensity = intensity
            uniforms.time = time
            uniforms.torchOrigin = origin
            uniforms.torchDistanceOffset = distanceOffset
            uniforms.torchBrightness = brightness
        }
        il.ratio = ratio
        co.ratio = ratio
    }
}
