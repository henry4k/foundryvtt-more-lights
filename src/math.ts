const floor = Math.floor
const cos   = Math.cos

export const clamp = (x: number, minVal: number, maxVal: number): number =>
    x >= minVal
        ? x <= maxVal
            ? x
            : maxVal
        : minVal

/**
 * The mix function returns the linear blend of x and y.
 *
 * @returns [x,y]
 */
export const mix = (x: number, y: number, t: number): number => x * (1.0 - t) + y * t

/**
 * The fract function returns the fractional part of x.
 *
 * @returns [0,1)
 */
const fract = (x: number): number => x - floor(x)

/**
 * The smoothstep function returns 0.0 if x is smaller than edge0 and 1.0 if x
 * is larger than edge1. Otherwise the return value is interpolated between 0.0
 * and 1.0 using Hermite polynomials.
 *
 * @returns [0,1]
 */
const smoothstep = (edge0: number, edge1: number, x: number): number => {
    if(x <= edge0)
        return 0.0
    if(x >= edge1)
        return 1.0
    x = (x - edge0) / (edge1 - edge0)
    return x * x * (3.0 - 2.0 * x)
}

const dot = (x1: number, y1: number, x2: number, y2: number): number => x1*x2 + y1*y2

/**
 * A conventional pseudo-random number generator with the "golden" numbers, based on uv position
 *
 * @returns [0,1)
 */
const random = (x: number, y: number): number => fract(cos(dot(x, y, 12.9898, 4.1414)) * 43758.5453)

/**
 * A conventional noise generator
 *
 * @returns [0,1)
 */
const noise = (x: number, y: number): number => {
    const bx = floor(x)
    const by = floor(y)
    const fx = smoothstep(0.0, 1.0, x-bx)
    const fy = smoothstep(0.0, 1.0, y-by)
    return mix(
        mix(random(bx+0.0, by+0.0), random(bx+1.0, by+0.0), fx),
        mix(random(bx+0.0, by+1.0), random(bx+1.0, by+1.0), fx),
        fy
    )
}

const binomial = (n: number, k: number) => {
    let coeff = 1
    let x
    for(x = n-k+1; x <= n; x++)
        coeff *= x
    for(x = 1; x <= k; x++)
        coeff /= x
    return coeff
}

export const fbmLimit = (octaves: number = 4, gain: number = 0.5): number => {
    let amplitude = 1.0
    let total = 0.0
    for(let i = 0; i < octaves; i++) {
        total += amplitude
        amplitude *= gain
    }
    return total
}

/**
 * Fractional Brownian Motion for a given number of octaves.
 *
 * @returns [0,limit) where limit depends on octaves and gain
 */
export const fbm = (x: number, y: number, octaves: number = 4, gain: number = 0.5): number => {
    let amplitude = 1.0
    let total = 0.0
    for(let i = 0; i < octaves; i++) {
        total += noise(x, y) * amplitude
        x += x
        y += y
        amplitude *= gain
    }
    return total
}

/**
 * Fractional Brownian Motion for a given number of octaves.
 *
 * This version gives results centered around zero.
 *
 * @returns (-limit,+limit) where limit depends on octaves and amplitude
 */
export const fbmZero = (x: number, y: number, octaves: number = 4, gain: number = 0.5): number => {
    let amplitude = 1.0
    let total = 0.0
    for(let i = 0; i < octaves; i++) {
        total += (noise(x, y) * 2.0 - 1.0) * amplitude
        x += x
        y += y
        amplitude *= gain
    }
    return total
}

type FbmFunction = (x: number, y: number) => number

export const createFbmFunction = ({octaves = 4,
                                   lacunarity = 2.0,
                                   gain = 0.5,
                                   amplitude = 1.0,
                                   frequency = 1.0}: {
                                   octaves: number,
                                   lacunarity: number,
                                   gain: number,
                                   amplitude: number,
                                   frequency: number
                                  }): FbmFunction => {
    return (x, y) => {
        let amplitude_ = amplitude
        let frequency_ = frequency
        let total = 0.0
        for(let i = 0; i < octaves; i++) {
            total += noise(x*frequency_, y*frequency_) * amplitude_
            frequency_ *= lacunarity
            amplitude_ *= gain
        }
        return total
    }
}
