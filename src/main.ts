import { torchLightAnimation } from './torch.js'
//import { radiantLightAnimation } from './radiant.js'

Hooks.once('init', () => {
    for(const lightAnimation of [torchLightAnimation]) {
        CONFIG.Canvas.lightAnimations[lightAnimation.label.toLowerCase().replace('.', '_')] = lightAnimation
    }
})
