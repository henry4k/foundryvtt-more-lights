export const moduleRoot = 'modules/more-lights'

export type LightAnimation = {
    label: string
    animation: CONFIG.Canvas.LightAnimationFunction
    illuminationShader?: ConstructorOf<AbstractBaseShader>
    colorationShader?: ConstructorOf<AbstractBaseShader>
}
